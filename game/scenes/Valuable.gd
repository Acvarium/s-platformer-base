extends "res://objects/spObjects/Interactable.gd"
export var price = 1
 
func _on_VArea2D_body_entered(body):
	if body.has_method("pickup_valuable"):
		body.pickup_valuable(price)
		queue_free()
