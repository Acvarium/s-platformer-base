extends Node2D
export var included_groups: String
export var included_methods: String
export var included_classes: String

var bodies = []
var included_groups_list = []
var included_methods_list = []
var included_classes_list = []

func form_included_lists():
	included_groups_list.clear()
	included_methods_list.clear()
	included_classes_list.clear()
	if included_groups != "":
		for l in included_classes.split(' '):
			included_groups_list.append(l)
	if included_methods != "":
		for l in included_methods.split(' '):
			included_methods_list.append(l)
	if included_classes != "":
		for l in included_classes.split(' '):
			included_classes_list.append(l)


func clear_list():
	bodies.clear()


func add_to_list(body):
	if body == get_parent():
		return
	var b_name = body.name
	if included_groups_list.size() != 0:
		var to_include = false
		for l in included_groups_list:
			if body.is_in_group(l):
				to_include = true
				break
		if !to_include:
			return
	if included_methods_list.size() != 0:
		var to_include = false
		for l in included_methods_list:
			if body.has_method(l):
				to_include = true
				break
		if !to_include:
			return
	if included_classes_list.size() != 0:
		var to_include = false
		for l in included_classes_list:
			if l == body.get_class():
				to_include = true
				break
		if !to_include:
			return
	bodies.append(weakref(body))

	
func remove_from_list(body):
	for i in range(bodies.size() - 1, -1, -1):
		if bodies[i].get_ref() == body:
			bodies.remove(i)

func get_bodies():
	return bodies
	
	
func _ready():
	form_included_lists()


func _on_Area2D_body_entered(body):
	add_to_list(body)


func _on_Area2D_body_exited(body):
	remove_from_list(body)
