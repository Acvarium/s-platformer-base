extends Node2D

export var room_size = Vector2(10, 8)
export var world_size = Vector2(4, 4) 
var world_data = [[],[]]
enum directions {LEFT, RIGHT, DOWN, UP}


func _ready():
	pass

func get_world_path_size():
	var n = 0
	for i in range(world_size.y):
		for j in range(world_size.x):
			if get_room(i, j) != '0':
				n += 1
	return n


func generate_world():
	randomize()
#------------clean world data
	world_data.clear()
	for i in range(world_size.y):
		var row = []
		for j in range(world_size.x):
			row.append('0')
		world_data.append(row)
	var enter = randi()%4
	
	world_data[0][enter] = 'X'
#----------------------------------
	var finished = false
	var current_cell = Vector2(enter, 0)
	while(!finished):
		var next_dir = get_dir(current_cell)
		var next_cell = current_cell
		if next_dir == directions.LEFT:
			next_cell = Vector2(current_cell.x - 1, current_cell.y)
		elif next_dir == directions.RIGHT:
			next_cell = Vector2(current_cell.x + 1, current_cell.y)
		elif next_dir == directions.DOWN:
			next_cell = Vector2(current_cell.x, current_cell.y + 1)
		if next_cell.y >= world_size.y:
			world_data[current_cell.y][current_cell.x] = 'E'
			finished = true
		else:
			current_cell = next_cell
			world_data[current_cell.y][current_cell.x] = '1'


func get_room(_x, _y):
	var _data = world_data[_y][_x]
	return _data


func gen_and_check():
	var n = 0
	while n < 6:
		generate_world()
		n = get_world_path_size()


func get_dir(cell):
	var gg = "\n" + get_world_ascii()
	if cell.x == 0:
		if get_room(cell.x + 1, cell.y) == '0':
			if randf() < 0.5:
				return directions.RIGHT
	elif cell.x == world_size.x - 1:
		if get_room(cell.x - 1, cell.y) == '0':
			if randf() < 0.5:
				return directions.LEFT
	else:
		if get_room(cell.x + 1, cell.y) == '0':
			if randf() < 0.5:
				return directions.RIGHT
		elif get_room(cell.x - 1, cell.y) == '0':
			if randf() < 0.5:
				return directions.LEFT
	return directions.DOWN


func get_world_ascii():
	var ss = ""
	for i in range(world_size.y):
		ss += '|'
		for j in range(world_size.x):
			if world_data[i][j] == '0':
				ss += '#'
			elif world_data[i][j] == 'X':
				ss += 'X'
			elif world_data[i][j] == '1':
				ss += ' '
			elif world_data[i][j] == 'E':
				ss += 'E'
		ss += '|'
		ss += '\n'
	return ss
