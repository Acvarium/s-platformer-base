extends Node2D


func _ready():
	if has_node("AnimatedSprite"):
		$AnimatedSprite.play("start")
	if has_node("CPUParticles2D"):
		$CPUParticles2D.emitting = true
	if has_node("AnimationPlayer"):
		$AnimationPlayer.play("start")

func _on_Timer_timeout():
	queue_free()
