extends KinematicBody2D
onready var gravity = ProjectSettings.get("physics/2d/default_gravity") * 15.0
export var mass = 10.0
onready var main_node: Node2D = get_tree().get_root().get_node("main")
export var is_debug_object = false
var velocity = Vector2.ZERO
var realvelocity = Vector2()
const FLOOR_NORMAL = Vector2.UP
var mode = 1
var is_dropped = false
var is_thrown = false
onready var last_pos = global_position
var throw_timeout_started = false

func get_collision_shape_size():
	for o in get_children():
		if o.get_class() == "CollisionShape2D":
			if o.shape.get_class() == "CircleShape2D":
				return o.shape.radius
			elif o.shape.get_class() == "RectangleShape2D":
				return o.shape.extents.x


func set_mode(_mode):
	mode = _mode
	set_collision_layer_bit(7, mode != 0)
	set_collision_mask_bit(7, false)


func _ready():
	if is_debug_object:
		get_collision_shape_size()


func drop():
	is_dropped = true
	is_thrown = true
	if has_node("ThrowTimeout"):
		$ThrowTimeout.start()
		throw_timeout_started = true


func fast_exp():
	pass


func hit(dir: Vector2, force: float):
	velocity = dir.normalized() * force
	drop()


func explosive_hit(dir: Vector2, force: float):
	velocity = dir.normalized() * force
	if has_node("ExpTimer"):
		if $ExpTimer.wait_time > 0.1:
			fast_exp()


func collision_processing(collision):
#	if is_debug_object:
#		print(velocity.length())
	if collision.collider.is_in_group("platform"):
		if velocity.length() < 100:
			velocity = move_and_slide(velocity, Vector2.UP) * 0.6
		else:
			velocity = velocity.bounce(collision.normal) * 0.6
	else:
		velocity = velocity.bounce(collision.normal) * 0.6

func _physics_process(_delta):
	$Ray.cast_to = velocity * 0.1
	if is_thrown and !is_dropped and realvelocity.length() < 0.1 and !throw_timeout_started:
		is_thrown = false
	if is_dropped:
		is_dropped = false
	if mode == 1:
		velocity.y += gravity * _delta
		velocity *= 0.99
		var snap_vector = Vector2.ZERO
		var collision: KinematicCollision2D = move_and_collide(velocity * _delta)
		if collision:
			collision_processing(collision)
	
	realvelocity = global_position - last_pos
#	if is_debug_object:
#		print(realvelocity.length())
	last_pos = global_position


func getvelocity():
	return velocity


func collided(vel):
	velocity = vel * 0.9


func _on_Area2D_body_entered(body):
	if body == self:
		return
	var body_name = body.name
	var o_name = name
	if mode == 1 and body.has_method("getvelocity"):
		if !is_thrown and body.mode == 1:
			var bodyvelocity = body.getvelocity()
			collided(bodyvelocity)


func _on_ThrowTimeout_timeout():
	throw_timeout_started = false
