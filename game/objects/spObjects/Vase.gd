extends "res://objects/spObjects/Interactable.gd"


func _ready():
	pass


func hit(vel: Vector2, force:float):
	to_break()

	
func collision_processing(collision):
	if collision.collider.has_method("hit"):
		pass
	else:
		if realvelocity.length() > 3.5:
			to_break()
#		velocity = velocity.bounce(collision.normal) * 0.6
	velocity = velocity.bounce(collision.normal) * 0.3


func collided(vel):
	if vel.length() > 220.0:
		to_break()
	
func to_break():
	main_node.add_effect("StarExp", $EffectCenter.global_position)
	queue_free()


func explosive_hit(dir: Vector2, force: float):
	if force > 300:
		to_break()
