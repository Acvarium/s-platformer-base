extends "res://objects/spObjects/Interactable.gd"
const exp_timeout_value = 3.0
var explosion_timeout = exp_timeout_value
var timer_started = false
var max_exp_dist = 200.0
var exp_force = 1200.0


func _ready():
	pass

func fast_exp():
	if explosion_timeout > 0.1:
		explosion_timeout = 0.1

func explosion_effect():
#	$AnimationPlayer.play("explode")
	set_mode(0)
	main_node.add_explosion_effect(global_position, 0)
	$ExpTimer.start()


func explode():
	main_node.explosion(global_position, 1.0)
	var exp_bodies = $AreaListManager.get_bodies()
	for b in exp_bodies:
		var body = b.get_ref()
		if body != null:
			var body_pos = body.global_position
			if body.has_method("get_center"):
				body_pos = body.get_center()
			var b_dist = global_position.distance_to(body_pos)
			if b_dist < max_exp_dist:
				var exp_vec = (body_pos - global_position).normalized()
				var fade_value = 1.0 - (b_dist / max_exp_dist)
				print(fade_value)
				print(exp_force * fade_value)
				body.explosive_hit(exp_vec, exp_force * fade_value)
	queue_free()


func _physics_process(delta):
	$Label.text = str("%.2f" % explosion_timeout)
	if explosion_timeout > 0.0:
		if timer_started:
			explosion_timeout -= delta
			if explosion_timeout <= 0.0:
				explosion_timeout = 0.0
				explosion_effect()
				timer_started = false
	if is_dropped and !timer_started:
		is_dropped = false
		timer_started = true
		explosion_timeout = exp_timeout_value


func _on_ExpTimer_timeout():
	explode()



