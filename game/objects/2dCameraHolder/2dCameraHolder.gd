extends Node2D
export(NodePath) var camera_path
onready var camera_obj : Camera2D = get_node(camera_path)

func _ready():
	pass

func update_camera_pos(delta):
	
	camera_obj.global_position = \
		camera_obj.global_position.linear_interpolate(global_position, delta * 5.0)
	
