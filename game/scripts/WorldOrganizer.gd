extends Node2D
export var to_generate_on_start = true
export(NodePath) var test_pointer_path
export(NodePath) var map_origin_path
onready var main_node: Node2D = get_tree().get_root().get_node("main")
#stones, ladders, ropes
var layers = [
	["ground"],
	["Bridge"],
	["Ladder"],
	["Rope"]]
var layer_ids = []


func cell_test(map_id, _pos, to_clear = true):
	var cell_pos = $TestMaps.get_child(map_id).world_to_map(_pos)
	cell_test_cell(map_id, cell_pos, to_clear)


func cell_test_cell(map_id, cell_pos, to_clear = true):
	if to_clear:
		$TestMaps.get_child(map_id).clear()
	$TestMaps.get_child(map_id).set_cellv(cell_pos, 0)


func _ready():
	layer_ids.clear()
	for i in range(layers.size()):
		layer_ids.append([])
	if to_generate_on_start and get_node(map_origin_path) != null:
		generate_world()


func fire_rope(_pos, hight, go_down = false):
	var stert_cell_pos = $Rope.world_to_map(_pos)
	main_node.set_test_point_pos(1, $Stone.map_to_world(stert_cell_pos))
	main_node.set_test_point_pos(0, _pos)
	
	var s_cell = $Stone.get_cellv(stert_cell_pos)
	if s_cell != -1:
		return
	var k = hight
	var top = stert_cell_pos.y
	var bot = stert_cell_pos.y
	for i in range(hight):
		var tc = Vector2(stert_cell_pos.x, stert_cell_pos.y - i)
		if $Stone.get_cellv(tc) != -1:
			break
		k -= 1
		top = stert_cell_pos.y - i
	if k != 0:
		for i in range(k + 1):
			var tc = Vector2(stert_cell_pos.x, stert_cell_pos.y + i)
			if $Stone.get_cellv(tc) != -1:
				break
			bot = stert_cell_pos.y + i 
	for i in range(bot, top - 1, -1):
		$Rope.set_cell(stert_cell_pos.x, i, 2)
	

func explosion(_pos, exp_force):
	var exp_cell: Vector2 = $Stone.world_to_map(_pos)
	var cell_size = $Stone.cell_size.x
	var cell_center = $Stone.map_to_world(exp_cell) + Vector2(cell_size / 2.0, cell_size / 2.0)
	main_node.set_test_point_pos(2, cell_center)
	var exp_cell_radius = 2
	var exp_radius = cell_size * exp_cell_radius
	var clear_test_map = true
	for i in range(exp_cell_radius * 2 + 3):
		for j in range(exp_cell_radius * 2 + 3):
			
			var c = exp_cell + Vector2(i - exp_cell_radius - 1, j - exp_cell_radius - 1)
			var c_pos = $Stone.map_to_world(c) + Vector2(cell_size / 2.0, cell_size / 2.0)
			var d = c_pos.distance_to(_pos)
			if  d < exp_radius: 
				$Stone.set_cellv(c, -1)
				cell_test_cell(1, c, clear_test_map)
				clear_test_map = false
	$Stone.update_bitmask_region()
	

func generate_world():
	for tm in get_children():
		if tm.get_class() == "TileMap":
			tm.clear()
	var map_origin: TileMap = get_node(map_origin_path)
	var tile_set: TileSet = map_origin.tile_set
#	tile_set.tile_get_name(
	for l in layer_ids:
		l.clear()
		
	for id in tile_set.get_tiles_ids():
		var tile_name = tile_set.tile_get_name(id)
		var matched = false
		for i in range(layers.size()):
			for j in range(layers[i].size()):
				if tile_name == layers[i][j]:
					matched = true
					layer_ids[i].append(id)
					break
			if matched:
				break
	
	for t in map_origin.get_used_cells():
		var cell_id: int = map_origin.get_cellv(t)
		var cell_flip_x = map_origin.is_cell_x_flipped(t.x, t.y)
		var cell_flip_y = map_origin.is_cell_y_flipped(t.x, t.y)
		var cell_transposed = map_origin.is_cell_transposed(t.x, t.y)
		
		for i in range(layer_ids.size()):
			for j in range(layer_ids[i].size()):
				if cell_id == layer_ids[i][j]:
					var current_map: TileMap = get_child(i)
					current_map.set_cellv(t, cell_id, cell_flip_x, cell_flip_y, cell_transposed)
			get_child(i).update_bitmask_region()
	map_origin.queue_free()

