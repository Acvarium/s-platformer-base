extends Node2D
var exp0 = load("res://objects/ExplosionA.tscn")
var star_exp = load("res://objects/StarExp.tscn")


func add_explosion_effect(_pos, exp_type = 0):
	if exp_type == 0:
		var exp_obj = exp0.instance()
		add_child(exp_obj)
		exp_obj.global_position = _pos

func add_effect(effect_name, _pos):
	if effect_name == "StarExp":
		var star_exp_obj = star_exp.instance()
		add_child(star_exp_obj)
		star_exp_obj.global_position = _pos
	
func _ready():
	pass
