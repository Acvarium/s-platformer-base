class_name Player
extends KinematicBody2D
onready var main_node: Node2D = get_tree().get_root().get_node("main")
export var speed = Vector2(180.0, 520.0)
onready var gravity = ProjectSettings.get("physics/2d/default_gravity") * 15.0
export(String) var action_suffix = ""
const FLOOR_DETECT_DISTANCE = 10.0
var velocity = Vector2.ZERO
var cutoffvelocity = 1200.0
const max_safe_fall_speed = 800.0
const FLOOR_NORMAL = Vector2.UP
var jump_boo = false
var jump_start_time = 0.0
var jump_start_pos = Vector2()
var face_dir = 1
var life = 0.5
var life_recover_speed = 0.1
var min_move = 0.0001
var ledge_offset = Vector2(12.0, 45.0)
enum anim_status {STAND, WALK, RUN, JUMP, FALL, HANG, 
ROPE, ROPE_UP,
LOOK_UP
}
onready var default_layer_mask = collision_layer
onready var HOLDER_DEF_POS = $Holder.position

var bomb = load("res://objects/spObjects/Bomb.tscn")

var anim_state_changed = false
const u_timeout = 0.1
enum rise_types {NONE, ROPE, LADDER}
var anim_state = anim_status.STAND
var is_on_ledge = false
var pd_pos = 35.0
var is_on_rope: bool = false
var is_holring_to_rope = false
var rope_jump_timeout = 0.0
var rise_type = rise_types.NONE
var is_crouching = false
var bridge_drop_timeout = 0.0
var throw_force = 1050
var throwvelocity = Vector2(1000.0, -300)
var throw_upvelocity = Vector2(1000.0, -2000)
var crouching_dropvelocity = Vector2(1.0, 0)
var drop_force = 100.0
var pickable_in_area = []
var object_in_hands = null
var quick_action_object = null
var prevvelocity = Vector2()
var is_whipping = false
var hit_force = 1000.0
var v_press_start = INF

var money = 0

const camera_v_move_timeout = 1000.0
const camera_v_move_dist = 200.0


func _ready():
	$AnimatedSprite.play("default")
	for hr in $Rays.get_children():
		for r in hr.get_children():
			r.add_exception(self)
	main_node.update_player_money_view(money)

func update_collision_masks():
	var __is_on_floor = _is_on_floor()
	set_collision_layer_bit(3, __is_on_floor and bridge_drop_timeout <= 0)
	set_collision_mask_bit(3, __is_on_floor and bridge_drop_timeout <= 0)


func get_wall_col_distance():
	var col_offset = 0.0
	if face_dir == -1:
		if $Rays/WallsDetector/WLeft.is_colliding():
			col_offset = ($Rays/WallsDetector/WLeft.global_position - \
				$Rays/WallsDetector/WLeft.get_collision_point()).x
	else:
		if $Rays/WallsDetector/WRight.is_colliding():
			col_offset = ($Rays/WallsDetector/WRight.global_position - \
				$Rays/WallsDetector/WRight.get_collision_point()).x
	return col_offset


func is_on_bridge():
	var is_on_platform = $Rays/Platform/PlatformDetector.is_colliding() or $Rays/Platform/PlatformDetector2.is_colliding()
	if is_on_platform:
		return !$Rays/Bridge/BridgeDetector.is_colliding() and !$Rays/Bridge/BridgeDetector2.is_colliding()
	return false


func update_life_view():
	main_node.show_life(life)


func get_ledge_point(_pos, _colider, is_on_right):
	if _colider.get_class() != "TileMap":
		return
	var tm: TileMap = _colider
	var cell = tm.world_to_map(_pos)
	var ledge_pos = tm.map_to_world(cell)
	if !is_on_right:
		ledge_pos.x += tm.cell_size.x
	main_node.set_test_point_pos(0, ledge_pos)
	return ledge_pos


func _input(event):
	if event.is_action_pressed("bomb"):
		throw_bomb()


func update_colider_state():
	$c1.disabled = is_crouching
	$c2.disabled = !is_crouching
	var pd_ray_pos = 10
	if is_crouching:
		pd_ray_pos = 15
	$Rays/Platform/PlatformDetector.position.x = -pd_ray_pos
	$Rays/Platform/PlatformDetector2.position.x = pd_ray_pos


func throw_bomb():
	var bomb_obj: Node2D = bomb.instance()
	var effects_node: Node2D = main_node.get_effects_node()
	effects_node.add_child(bomb_obj)
	bomb_obj.global_position = $Holder.global_position
	pickup(bomb_obj, true)
	throw(true)


func hit(value):
	life -= value
	$Anim.play("hit")
	update_life_view()


func whip():
	$Whip.scale.x = face_dir
	$AnimatedSprite.playing = false
	$AnimatedSprite.animation = "whip"
	$Anim.play("whip")
	is_whipping = true
	if is_on_ledge:
		is_on_ledge = false
		rope_jump_timeout = u_timeout
		
		
func reset_cam():
	$CameraHolder.position = Vector2()
	v_press_start = INF

func v_move_camera(dir):
	if (OS.get_ticks_msec() - v_press_start) > camera_v_move_timeout and !is_holring_to_rope:
		anim_state = anim_status.LOOK_UP
		$CameraHolder.position.y =  dir * 200.0


func throw(quick_action = false):
	var drop_vector = throwvelocity.normalized() * throw_force
	if Input.is_action_pressed("move_up"):
		drop_vector = throw_upvelocity.normalized() * throw_force
	if is_crouching:
		drop_vector = crouching_dropvelocity.normalized() * drop_force
	drop_vector.x *= face_dir
	return drom_from_hands(drop_vector, quick_action)


func check_ledge(is_right_ledge):
	var ledge_obj = null
	if is_right_ledge:
		ledge_obj = $Rays/HookRays/hbr.get_collider()
	else:
		ledge_obj = $Rays/HookRays/hbl.get_collider()
	if ledge_obj:
		if ledge_obj.get_class() == "TileMap":
			return true
		
	return false

func _physics_process(_delta):
	
	if (Input.is_action_pressed("move_down" + action_suffix) or 
		Input.is_action_pressed("move_up" + action_suffix)):
		
		if (is_on_floor() or is_on_ledge) and v_press_start == INF:
			v_press_start = OS.get_ticks_msec()
		if Input.is_action_pressed("move_down" + action_suffix):
			v_move_camera(1)
		else:
			v_move_camera(-1)
			
	elif v_press_start != INF:
			reset_cam()
	
	
	main_node.cell_test(0, $PlayerCenter.global_position)
	if Input.is_action_just_pressed("rope"):
		main_node.fire_rope($PlayerCenter.global_position, 7)
	life += life_recover_speed * _delta
	life = clamp(life, 0.0, 1.0)
	if Input.is_action_just_pressed("hit"):
		if !throw():
			if is_crouching:
				pickup_near()
			else:
				whip()
		
	update_collision_masks()
	var run_pressed = Input.is_action_pressed("run" + action_suffix)
	if Input.is_action_pressed("move_down" + action_suffix):
		run_pressed = false
	var current_face_dir = face_dir
	var current_crouching_state = is_crouching
	var current_rise_type = rise_type
	is_crouching = false
	if bridge_drop_timeout > 0.0:
		bridge_drop_timeout -= _delta
	else:
		bridge_drop_timeout = 0.0
	if rope_jump_timeout > 0.0:
		rope_jump_timeout -= _delta
	else:
		rope_jump_timeout = 0.0
	var run_over = false
	var speed_boost = Vector2(1.0, 1.0)
	var is_running = false
	if run_pressed and !is_crouching:
		speed_boost = Vector2(2.0, 1.0)
		is_running = true
	if is_crouching:
		speed_boost = Vector2(0.6, 1.0)
	
	$Rays/pdExtra/pdExtraB.position.x = pd_pos * (-velocity.x) / (speed.x * 2.0)
	$Rays/pdExtra/pdExtraT.position.x = pd_pos * (-velocity.x) / (speed.x * 2.0)
	
	$Rays/pdExtra/pdExtraBFront.position.x = pd_pos * (velocity.x) / (speed.x * 1.2)
	$Rays/pdExtra/pdExtraTFront.position.x = pd_pos * (velocity.x) / (speed.x * 1.2)
	if velocity.y == 0 and is_running and $Rays/pdExtra/pdExtraBFront.is_colliding() and !$Rays/pdExtra/pdExtraTFront.is_colliding():
		run_over = true
	var right_ledge = !$Rays/HookRays/htr.is_colliding() and $Rays/HookRays/hbr.is_colliding()
	var left_ledge = !$Rays/HookRays/htl.is_colliding() and $Rays/HookRays/hbl.is_colliding()
	if right_ledge:
		right_ledge = check_ledge(true)
	elif left_ledge:
		left_ledge = check_ledge(false)
	
	if is_on_ledge and (!right_ledge and !left_ledge):
		is_on_ledge = false
		rope_jump_timeout = u_timeout
	var currentanim_state = anim_state
	if !is_holring_to_rope and !run_over:
		var m = velocity.y * velocity.y / (cutoffvelocity * cutoffvelocity)
		m = clamp(m, 0, 1)
		var fr = gravity * m 
		velocity.y += (gravity - fr) * _delta
	else:
		velocity.y = 0.0
	var direction = get_direction()
	if direction.x > 0:
		face_dir = 1
	elif direction.x < 0:
		face_dir = -1
	if is_on_rope:
		if Input.is_action_just_pressed("move_left" + action_suffix):
			face_dir = -1
		elif Input.is_action_just_pressed("move_right" + action_suffix):
			face_dir = 1
	
	if direction.x != 0 and is_holring_to_rope:
		is_holring_to_rope = false

	if Input.is_action_pressed("move_up" + action_suffix) and !is_on_ledge:

		catch_on_rope()
		if is_on_rope and $Rays/RopeTopRay/ropeRay.is_colliding():
			get_rope_over_pos()
			direction.y = -0.5
			is_holring_to_rope = true
	elif Input.is_action_pressed("move_down" + action_suffix):
		get_rope_over_pos()
		if get_rope_over_pos().x == -INF:
			is_holring_to_rope = false
		if is_holring_to_rope and _is_on_floor():
			is_holring_to_rope = false
		if is_on_rope and is_holring_to_rope:
			direction.y = 0.5
	var is_jump_interrupted = Input.is_action_just_released("jump" + action_suffix) and velocity.y < 0.0
	velocity = calculate_movevelocity(velocity, direction, speed * speed_boost, is_jump_interrupted)
	
# warning-ignore:unused_variable
	var snap_vector = Vector2.ZERO
	if direction.y == 0.0:
		snap_vector = Vector2.DOWN * FLOOR_DETECT_DISTANCE
	var is_on_platform = $Rays/Platform/PlatformDetector.is_colliding() or $Rays/Platform/PlatformDetector2.is_colliding()
	if !is_on_platform and (right_ledge or left_ledge):
		is_on_platform = true

	if is_on_rope:
		is_on_platform = true
	velocity = move_and_slide(velocity, FLOOR_NORMAL)
	if velocity.y > 0.000 and rope_jump_timeout <= 0:
		if face_dir > 0:
			if right_ledge:
				velocity = Vector2()
				is_on_ledge = true
				var col = $Rays/HookRays/hbr.get_collider()
				var ray_pos = $Rays/HookRays/hbr.global_position + $Rays/HookRays/hbr.cast_to
				var ledge_pos = get_ledge_point(ray_pos, col, true)
				var _offset = ledge_offset
				_offset.x = -_offset.x
				global_position = ledge_pos + _offset
		elif face_dir < 0:
			if left_ledge:
				velocity = Vector2()
				is_on_ledge = true
				var col = $Rays/HookRays/hbl.get_collider()
				var ray_pos = $Rays/HookRays/hbl.global_position + $Rays/HookRays/hbl.cast_to
				var ledge_pos = get_ledge_point(ray_pos, col, false)
				var _offset = ledge_offset
				global_position = ledge_pos + _offset
	
	if Input.is_action_pressed("move_down" + action_suffix): 
		if abs(velocity.y) <= min_move and !(is_on_rope and is_holring_to_rope):
			if _is_on_floor():
				is_crouching = true
	
	if direction.x != 0:
		anim_state = anim_status.WALK
		if is_running:
			anim_state = anim_status.RUN
	else:
		anim_state = anim_status.STAND
	if is_on_ledge:
		anim_state = anim_status.HANG
	if is_holring_to_rope:
		if velocity.y == 0:
			anim_state = anim_status.ROPE
		else:
			anim_state = anim_status.ROPE_UP
	elif velocity.y > min_move:
		anim_state = anim_status.FALL
	var state_changed = false
	if currentanim_state != anim_state:
		state_changed = true
	if current_crouching_state != is_crouching:
		update_colider_state()
		state_changed = true
	if current_face_dir != face_dir:
		state_changed = true
	if current_rise_type != rise_type:
		state_changed = true
	if anim_state_changed:
		anim_state_changed = false
		state_changed = true
	if state_changed:
		if anim_state == anim_status.STAND:
			if is_crouching:
				$AnimatedSprite.play("crouching_stand")
			else:
				$AnimatedSprite.play("default")
		elif anim_state == anim_status.WALK:
			if is_crouching:
				$AnimatedSprite.play("crouching_walk")
			else:
				$AnimatedSprite.play("walk")
			$AnimatedSprite.flip_h = face_dir == -1
		elif  anim_state == anim_status.RUN:
			$AnimatedSprite.play("run")
			$AnimatedSprite.flip_h = face_dir == -1
		elif anim_state == anim_status.HANG:
			$AnimatedSprite.play("hang")
			if $Rays/HookRays/hbr.is_colliding():
				face_dir = 1
				$AnimatedSprite.flip_h = false
			elif $Rays/HookRays/hbl.is_colliding():
				face_dir = -1
				$AnimatedSprite.flip_h = true
		elif anim_state == anim_status.ROPE:
			if rise_type == rise_types.LADDER:
				$AnimatedSprite.play("ladder_hold")
			else:
				$AnimatedSprite.play("rope_hold")
		elif anim_state == anim_status.ROPE_UP:
			if rise_type == rise_types.LADDER:
				$AnimatedSprite.play("ladder_up")
			else:
				$AnimatedSprite.play("rope_up")
		elif anim_state == anim_status.FALL:
			$AnimatedSprite.play("fall")
			$AnimatedSprite.flip_h = face_dir == -1
		elif anim_state == anim_status.LOOK_UP:
			$AnimatedSprite.play("look_up")
			$AnimatedSprite.flip_h = face_dir == -1
		
	update_life_view()
	if prevvelocity.y > velocity.y:
		var sp = abs(prevvelocity.y - velocity.y)
		if _is_on_floor() and !is_on_rope:
			if sp > max_safe_fall_speed:
				hit(clamp((cutoffvelocity - max_safe_fall_speed) / 
					(sp - max_safe_fall_speed), 0.0, 0.9))
	prevvelocity = velocity
	raycast_tester()
	$CameraHolder.update_camera_pos(_delta)
	if object_in_hands:
		$Holder.position = HOLDER_DEF_POS
		$Holder.position.x = 12 * face_dir
		var wall_offset = get_wall_col_distance()
		if abs(wall_offset) > 0.01:
			var obj_radius = object_in_hands.get_ref().get_collision_shape_size()
			if abs(wall_offset) < (12.0 + obj_radius):
				var new_holder_pos = HOLDER_DEF_POS
				new_holder_pos.x += (obj_radius * -face_dir)  - wall_offset
				$Holder.position = new_holder_pos
			
##-------------------------


func pickup_valuable(value):
	money += value
	update_modey()
	

func update_modey():
	main_node.update_player_money_view(money)

func get_rope_over_pos():
	rise_type = rise_types.NONE 
	var rope = null
	var j = -1
	for i in range($Rays/RopeRays.get_child_count()):
		var r: RayCast2D = $Rays/RopeRays.get_child(i)
		if r.is_colliding():
			rope = r.get_collider()
			j = i
			break
	if !rope:
		is_holring_to_rope = false
	var rope_pos: Vector2 = Vector2(-INF,0)
	if rope:
		if rope.is_in_group("ladder"):
			rise_type = rise_types.LADDER
		if rope.get_class() == "TileMap":
			var selected_ray: RayCast2D = $Rays/RopeRays.get_child(j)
			var ray_top_pos = selected_ray.global_position + selected_ray.cast_to
			
			var tm: TileMap = rope
			var cell = tm.world_to_map(ray_top_pos)
			rope_pos = tm.map_to_world(cell) + tm.cell_size / 2
		else:
			rope_pos = rope.global_position
	return rope_pos


func raycast_tester():
	for c in $TestDots.get_children():
		c.self_modulate = Color.white
	if $Rays/Platform/PlatformDetector.is_colliding():
		$TestDots/i4.self_modulate = Color.red
	if $Rays/Platform/PlatformDetector2.is_colliding():
		$TestDots/i6.self_modulate = Color.red
		
	if $Rays/Bridge/BridgeDetector.is_colliding():
		$TestDots/i5.self_modulate = Color.red
	if $Rays/Bridge/BridgeDetector2.is_colliding():
		$TestDots/i7.self_modulate = Color.red
		
	if $Rays/HookRays/hbl.is_colliding():
		$TestDots/i1.self_modulate = Color.red
		
	if $Rays/HookRays/hbr.is_colliding():
		$TestDots/i2.self_modulate = Color.red

	if $Rays/HookRays/htl.is_colliding():
		$TestDots/i0.self_modulate = Color.red

	if $Rays/HookRays/htr.is_colliding():
		$TestDots/i3.self_modulate = Color.red


func _is_on_floor():
	
	if $Rays/Platform/PlatformDetector.is_colliding() or $Rays/Platform/PlatformDetector2.is_colliding():
		return true
	if $Rays/pdExtra/pdExtraB.is_colliding() and !$Rays/pdExtra/pdExtraT.is_colliding():
		return true
	return false


func get_direction():
	var to_jump = 0.0
	var jump_just_pressed = Input.is_action_just_pressed("jump" + action_suffix)
	if jump_just_pressed and is_on_floor():
		bridge_drop_timeout = u_timeout * 1.8
#		rope_jump_timeout = u_timeout * 2.5
	if jump_just_pressed and Input.is_action_pressed("move_down" + action_suffix):
		if is_on_bridge():
			jump_just_pressed = false
			
	if (_is_on_floor() or is_holring_to_rope) and jump_just_pressed:
		rope_jump_timeout = u_timeout * 2.0
		to_jump = -1.0
		is_on_rope = false
		is_holring_to_rope = false
	if is_on_ledge and jump_just_pressed:
		is_on_ledge = false
		rope_jump_timeout = u_timeout
		if !Input.is_action_pressed("move_down" + action_suffix):
			to_jump = -1.0
		
	if to_jump < 0.0:
		jump_boo = true
		jump_start_time = OS.get_ticks_msec()
		jump_start_pos = position
	if is_on_ledge:
		return Vector2(0,0)
	var is_on_platform = $Rays/Platform/PlatformDetector.is_colliding() or $Rays/Platform/PlatformDetector2.is_colliding()
	
	if is_holring_to_rope and !is_on_platform:
		return Vector2(0, to_jump)
	return Vector2(
		Input.get_action_strength("move_right" + action_suffix) - Input.get_action_strength("move_left" + action_suffix),
		to_jump
	)


func catch_on_rope():
	var rope_pos = get_rope_over_pos()
	if rope_pos.x > -INF and rope_jump_timeout <= 0.0:
		var rope_x_pos = rope_pos.x
		global_position.x = rope_x_pos
		is_on_rope = true


func calculate_movevelocity(
		linearvelocity,
		direction,
		_speed,
		is_jump_interrupted
	):
	var velocity = linearvelocity
	velocity.x = _speed.x * direction.x
	if direction.y != 0.0:
		velocity.y = _speed.y * direction.y
	if is_jump_interrupted:
		velocity.y *= 0.6
	if is_on_ledge:
		return Vector2(0,0)
	return velocity


func get_closest_pickable():
	var dist = INF
	var index = -1
	for i in range(pickable_in_area.size()):
		var p: Node2D = pickable_in_area[i].get_ref()
		if p != null:
			var current_dist = global_position.distance_to(p.global_position)
			if current_dist < dist:
				dist = current_dist
				index = i
	if index != -1:
		return pickable_in_area[index].get_ref()
	return null


func drom_from_hands(vel, quick_action = false):
	var object_to_drop_ref = object_in_hands
	if quick_action:
		object_to_drop_ref = quick_action_object
	if object_to_drop_ref != null:
		var object_to_drop = object_to_drop_ref.get_ref()
		if object_to_drop != null:
			if !quick_action:
				$Holder.remote_path = ""
			object_to_drop.set_mode(1)
			object_to_drop.velocity = vel + velocity
			if object_to_drop.has_method("drop"):
				object_to_drop.drop()
			if !quick_action:
				object_in_hands = null
			else:
				object_to_drop = null
			return true
	return false


func pickup_near():
	var object_to_pickup = get_closest_pickable()
	if object_to_pickup != null:
		pickup(object_to_pickup)


func pickup(body, quick_action = false):
	body.set_mode(0)
	body.global_position = $Holder.global_position
	if !quick_action:
		$Holder.remote_path = body.get_path()
		object_in_hands = weakref(body)
	else:
		quick_action_object = weakref(body)


func remove_from_pickable(body):
	for i in range(pickable_in_area.size() - 1, -1, -1):
		if pickable_in_area[i].get_ref() == body:
			pickable_in_area.remove(i)


func get_center():
	return $PlayerCenter.global_position


func clean_pickable():
	for i in range(pickable_in_area.size - 1, -1, -1):
		if pickable_in_area[i].get_ref() == null:
			pickable_in_area.remove(i)


func _on_PickupArea_body_entered(body):
	if body.is_in_group("pickable"):
		pickable_in_area.append(weakref(body))


func _on_PickupArea_body_exited(body):
	remove_from_pickable(body)


func _on_Anim_animation_finished(anim_name):
	if anim_name == "whip":
		$AnimatedSprite.animation = "default"
		$Anim.play("_reset")
		anim_state_changed = true
		is_whipping = false
		$Whip.clean_whip_data()
