extends Node2D

var  whip_objects = []
const hit_point_offset = Vector2(0, 1)


func clean_whip_data():
	whip_objects.clear()
	
	
func _ready():
	pass


func whip_object(obj):
	var n = obj.name
	if obj.has_method("hit"):
		for o in whip_objects:
			if o.get_ref() == obj:
				return
		var player_center = get_parent().get_center()
		var hit_vec = (obj.global_position - player_center).normalized()
		hit_vec = (hit_vec + hit_point_offset).normalized()
		obj.hit(hit_vec, get_parent().hit_force)
		whip_objects.append(weakref(obj))


func _on_BackArea2D_area_entered(area):
	var obj = area.get_parent()
	if obj != get_parent():
		whip_object(obj)


func _on_MidArea2D_area_entered(area):
	var obj = area.get_parent()
	if obj != get_parent():
		whip_object(obj)


func _on_FrontArea2D_area_entered(area):
	var obj = area.get_parent()
	if obj != get_parent():
		whip_object(obj)
