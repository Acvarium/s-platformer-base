extends Node2D
export(NodePath) var world_organizer_path
export(NodePath) var effects_path
onready var world_organizer = get_node(world_organizer_path)
onready var effects = get_node(effects_path)


func _ready():
	Input.set_mouse_mode(Input.MOUSE_MODE_HIDDEN)
	generate_world()


func get_effects_node():
	return effects


func generate_world():
	$WorldGenerator.gen_and_check()
	var ASCII_world = $WorldGenerator.get_world_ascii()
	$CanvasLayer/Control/MapView.text = ASCII_world


func add_effect(effect_name, _pos):
	effects.add_effect(effect_name, _pos)


func add_explosion_effect(_pos, exp_type = 0):
	effects.add_explosion_effect(_pos, exp_type)


func explosion(_pos, exp_force):
	if world_organizer:
		world_organizer.explosion(_pos, exp_force)

func update_player_money_view(money, player_id = 0):
	if player_id == 0:
		$CanvasLayer/Control/Gold.text = "$" + str(money)

func fire_rope(_pos, hight, go_down = false):
	if world_organizer:
		world_organizer.fire_rope(_pos, hight, go_down)


func show_life(value):
	var l = value * $CanvasLayer/Control/Life.max_value
	$CanvasLayer/Control/Life.value = l


func _process(delta):
	$CanvasLayer/Control/Label.text = str(Engine.get_frames_per_second())


func _input(event):
	if event.is_action_pressed("map_gen"):
		generate_world()


func set_test_point_pos(point_id, pos):
	$TestPoints.get_child(point_id).global_position = pos


func cell_test(map_id, _pos):
	world_organizer.cell_test(map_id, _pos)
